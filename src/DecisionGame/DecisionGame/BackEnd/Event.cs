﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// This class represents events happening in the game
    /// </summary>
    /// <description>
    /// It is later inherited by RandomEvent, by itself it reptresents events happening always in the same rounds.
    /// </description>
    public class Event {
        public List<Action> Actions = new List<Action>();

        public Event(List<Action> actions) {
            AddActions(actions);
        }

        public void AddActions(List<Action> actions) {
            foreach (var act in actions) Actions.Add(act);
        }
        public void ExecuteActions() {
            foreach (var act in Actions) act();
        }

        public static void ShowMessage(string msg) {
            MessageBox.Show(msg);
        }

        public static void AddMoney(int money, Hotel hotel) {
            hotel.Money += money;
        }

        public static void AddPopularity(int popularity, Hotel hotel) {
            hotel.Popularity += popularity;
        }

        public static void AddMonthlyPayments(int payment, Hotel hotel) {
            hotel.MonthlyPayments += payment;
        }

        public static void AddRoomsCost(int cost) {
            RoomStandard.CostPerDay += cost;
            RoomDelux.IncomePerDay += cost;
        }
    }
}