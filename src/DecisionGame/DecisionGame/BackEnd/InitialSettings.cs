﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace DecisionGame.BackEnd {
    public class InitialSettings {
        /// <summary>
        /// Class representing starting settings of the game. 
        /// </summary>
        public Dictionary<string, string> jsonSettings;
        public Place Place = Place.None;
        public string PlayerName;

        public InitialSettings() {
            var json = File.ReadAllText("settings.json");
            jsonSettings = JsonSerializer.Deserialize<Dictionary<string, string>>(json);
        }
    }
}