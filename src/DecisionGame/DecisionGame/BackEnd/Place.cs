namespace DecisionGame.BackEnd {
    /// <summary>
    /// Simple enum prepresenting possible locations of the hotel. 
    /// </summary>
    public enum Place {
        None,
        Suburbs,
        MiddleCity,
        Center
    }
}