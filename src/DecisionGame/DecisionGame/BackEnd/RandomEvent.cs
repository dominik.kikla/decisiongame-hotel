using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace DecisionGame.BackEnd {
    public class RandomEvent : Event {
        /// <summary>
        /// This class is responsible for random events happening throughout the game .
        /// </summary>
        /// <description>
        /// It inherits from Event class. We define all the random events possible here.
        /// </description>
        public readonly double Probability;
        public readonly int TimesItCanHappen;
        public int TimesItHappened = 0;

        private RandomEvent(List<Action> actions, int number) : base(actions) {
            var json = File.ReadAllText("eventSettings.json");
            var eventSettings = JsonSerializer.Deserialize<List<Dictionary<string, string>>>(json);
            if (eventSettings == null) return;
            Probability = double.Parse(eventSettings[number]["Probability"]);
            TimesItCanHappen = int.Parse(eventSettings[number]["TimesItCanHappen"]);
        }

        public static List<RandomEvent> GenerateRandomEvents(Hotel hotel) {
            return new List<RandomEvent> {
                // Random donation
                new RandomEvent(new List<Action> {
                    () => ShowMessage("Anonimowy wielbiciel zdecydował sie wesprzeć Twój hotel sporą kwotą pieniężną!"),
                    () => AddMoney(5000, hotel)
                }, 0),
                // Random accident
                new RandomEvent(new List<Action> {
                    () => ShowMessage("Wydarzył się wypadek! Musisz pokryć koszty napraw..."),
                    () => AddMoney(-10000, hotel)
                }, 1),
                // Good review
                new RandomEvent(new List<Action> {
                    () => ShowMessage("Znany krytyk napisał pozytywną recenzję na temat Twojego hotelu, popularność wzrasta."),
                    () => AddPopularity(25, hotel)
                }, 2),
                // Bad review
                new RandomEvent(new List<Action> {
                    () => ShowMessage("Znany krytyk napisał negatywną recenzję na temat Twojego hotelu, popularność spada."),
                    () => AddPopularity(-25, hotel)
                }, 3),
                // Bankroll from city
                new RandomEvent(new List<Action> {
                    () => ShowMessage("Dostajesz duże dofinansowanie z miasta."),
                    () => AddMoney(40000, hotel)
                }, 4),
                // City found out about mistakes in taxes
                new RandomEvent(new List<Action> {
                    () => ShowMessage("Okazuje się, że w zeznaniach podatkowych Twojego hotelu wykryte zostały niezgodności, musisz zapłacić karę."),
                    () => AddMoney(-15000, hotel)
                }, 5),
                // Good opinions from workers
                new RandomEvent(new List<Action> {
                    () => ShowMessage("Twoi pracownicy pozytywnie wypowiadają się o pracy w hotelu, popularność wzrasta."),
                    () => AddPopularity(10, hotel)
                }, 6),
                // Scandal with worker
                new RandomEvent(new List<Action> {
                    () => ShowMessage(
                        "W wyniku niedawnego skandalu związanego z wrogo nastawionym pracownikiem musisz zapłacić rekompensatę, a popularność hotelu spada"),
                    () => AddPopularity(-15, hotel),
                    () => AddMoney(-3500, hotel)
                }, 7)
            };
        }
    }
}