namespace DecisionGame.BackEnd {
    /// <summary>
    /// Abstract class representing hotel room.  
    /// </summary>
    /// <description>
    /// It is inherited by RoomDelux and RoomStandard.
    /// </description>
    public abstract class Room {
        public int DaysReserved;
        public abstract int GetIncomePerDay();
        public abstract int GetCostPerDay();
        public abstract int GetBuyCost();
        public abstract int GetCompetitorCost();
        public static RoomStandard[] GetStartingRooms() {
            return new[] {
                new RoomStandard(),
                new RoomStandard(),
                new RoomStandard(),
                new RoomStandard(),
                new RoomStandard()
            };
        }
        public static int CalculateCostPerRoom(int price, int roomsCount) {
            return (int) (price * (1 - roomsCount / 20 * 0.05));
        }
    }
}