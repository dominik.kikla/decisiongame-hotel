namespace DecisionGame.BackEnd {
    public class RoomDelux : Room {
        /// <summary>
        /// Class representing certain type of room - room Delux 
        /// </summary>
        /// <description>
        /// Rooms Delux are more expensive than other types of rooms.
        /// </description>
        public static int IncomePerDay;
        public static int CostPerDay;
        public static int BuyCost;
        public static int CompetitorCost;

        public override int GetIncomePerDay() {
            return IncomePerDay;
        }
        public override int GetCostPerDay() {
            return CostPerDay;
        }
        public override int GetBuyCost() {
            return BuyCost;
        }
        public static int StaticIncomePerDay() {
            return IncomePerDay;
        }
        public static int StaticCostPerDay() {
            return CostPerDay;
        }
        public static int StaticBuyCost() {
            return BuyCost;
        }
        public override int GetCompetitorCost()
        {
            return CompetitorCost;
        }
    }
}