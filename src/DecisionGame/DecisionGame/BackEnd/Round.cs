﻿using System.Collections.Generic;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// Class representing a round in the game.
    /// </summary>
    /// <description>
    /// It keeps track of events happening during it and decisions possible.
    /// </description>
    public class Round {
        public List<Decision> Decisions = new List<Decision>();
        public List<Event> Events = new List<Event>();
        public int RoundNumber;

        public Round(int n, List<Event> evntList = null, List<Decision> decList = null) {
            RoundNumber = n;
            if (evntList != null)
                foreach (var evnt in evntList)
                    Events.Add(evnt);

            if (decList != null)
                foreach (var dec in decList)
                    Decisions.Add(dec);
        }
    }
}