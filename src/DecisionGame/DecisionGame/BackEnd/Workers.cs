using System;
using System.Collections.Generic;
using System.Linq;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// Simple Enum representing all professions that workers can have.
    /// </summary>
    /// <description>
    /// Only 1 profession per worker.
    /// </description>
    public enum Profession {
        Gardener = 0, // NIE ZMIENIAC, KONTROLKA W AD AGENCY FORM BAZUJE NA TYM
        Receptionist,
        Cleaner,
        Cook,
        BellBoy,
        Janitor
    }
    /// <summary>
    /// Class responsible for managing workers and their payments. 
    /// </summary>
    public class Workers {
        public Dictionary<Profession, int> workersCount;
        public Dictionary<Profession, int> wages;
        public Dictionary<Profession, int> hapiness;
        public string[] names;
        public static int minimumWage;
        public static double averageWage;
        public static int defaultMonthlyPayment;
        public static int defaultHappiness;
        public Workers() {
            workersCount = new Dictionary<Profession, int> {
                {Profession.Cook, 0},
                {Profession.Cleaner, 0},
                {Profession.Gardener, 0},
                {Profession.BellBoy, 0},
                {Profession.Receptionist, 0},
                {Profession.Janitor, 0},
            };

            wages = new Dictionary<Profession, int> {
                {Profession.Cook, defaultMonthlyPayment},
                {Profession.Cleaner, defaultMonthlyPayment},
                {Profession.Gardener, defaultMonthlyPayment},
                {Profession.BellBoy, defaultMonthlyPayment},
                {Profession.Receptionist, defaultMonthlyPayment},
                {Profession.Janitor, defaultMonthlyPayment},
            };

            hapiness = new Dictionary<Profession, int> {
                {Profession.Cook, defaultHappiness},
                {Profession.Cleaner, defaultHappiness},
                {Profession.Gardener, defaultHappiness},
                {Profession.BellBoy, defaultHappiness},
                {Profession.Receptionist, defaultHappiness},
                {Profession.Janitor, defaultHappiness},
            };

            names = new string[] {
                "Ogrodnik",
                "Recepcjonista",
                "Sprz?tacz",
                "Kucharz",
                "Tragarz",
                "Dozorca"
            };
        }

        public int calculateHappinessOf(Profession profession) {
            return (int) (100 + (wages[profession] - averageWage) * 100 / averageWage);
        }

        public int calculateWeightedHapinessOfAll() {
            int sum = Enum.GetValues(typeof(Profession)).Cast<object>().Sum(profession => wages[(Profession) profession] * workersCount[(Profession) profession]);
            return sum / Enum.GetValues(typeof(Profession)).Cast<object>().Sum(profession => workersCount[(Profession) profession]);
        }

        public void HireWorkers(Profession profession, int numberOfWorkers) {
            workersCount[profession] += numberOfWorkers;
        }

        public bool FireWorkers(Profession profession, int numberOfWorkers) {
            if (workersCount[profession] <= numberOfWorkers) return false;
            workersCount[profession] -= numberOfWorkers;
            return true;
        }
    }
}