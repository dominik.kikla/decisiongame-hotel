﻿using DecisionGame.FrontEnd;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// One of the classes representing bookmarks visible in game. 
    /// </summary>
    /// <description>
    /// It represents player's hotel. In-game window allows user to change price of owned rooms.
    /// </description>
    public class Hotel {
        public readonly List<Room> Rooms = new List<Room>();

        private int employeesCount;
        private int money;
        public int monthlyPayments;
        public int OldMoney;

        public Game parent;
        public int Popularity;

        public HotelForm hotelForm => parent.parentForm.hotelForm;

        public Hotel(Game Parent) {
            parent = Parent;
            Rooms.AddRange(Room.GetStartingRooms());
            EmployeesCount = 0;
        }

        public int MonthlyPayments
        {
            get => monthlyPayments;
            set
            {
                monthlyPayments = value;
                parent.parentForm.ChangedMonthlyPaymentsEvent(value);
            }
        }

        public int EmployeesCount {
            get => employeesCount;
            set {
                employeesCount = value;
                parent.parentForm.ChangedEmployeesCountEvent(value);
            }
        }

        public int Money {
            get => money;
            set {
                money = value;
                parent.parentForm.ChangedMoneyEvent();
                parent.parentForm.buyRoomForm.ChangedMoneyEvent();
            }
        }

        public void AdvanceDay() {
            AdvanceRoomReservationsAndGetMoney();

            AllocateNewClients(Clients.GetClientsReservations(Popularity, this.parent.CompetitionCoefficient));
        }
        private void AdvanceRoomReservationsAndGetMoney() {
            foreach (var room in Rooms.Where(room => room.DaysReserved > 0)) {
                room.DaysReserved -= 1;
                Money += room.GetIncomePerDay() - room.GetCostPerDay();
            }
        }

        private void AllocateNewClients(int[] daysToReserve) {
            var freeRooms = Rooms.Where(room => room.DaysReserved == 0).Select(r => r).ToArray();
            for (var i = 0; i < daysToReserve.Length && i < freeRooms.Length; ++i)
                freeRooms[i].DaysReserved = daysToReserve[i];
        }

        public void BuyRooms(Room newRoom, int numberOfRooms, int costPerRoom) {
            Money -= costPerRoom * numberOfRooms;

            parent.AllRounds[parent.CurrentRound.RoundNumber + 1].Events.Add(
                new Event(new List<Action> {
                    // Info powitalne
                    () => BuyRoomsDelegate(newRoom, numberOfRooms)
                })
            );
        }

        public void PrepareForNextRound()
        {
            hotelForm.PrepareForNextRound();
        }

        public void makeChangePricePerRoomDecision(int newPrice)
        {
            RoomStandard.CostPerDay = newPrice;
        }

        public void makeChangePricePerRoomDeluxDecision(int newPrice)
        {
            RoomDelux.CostPerDay = newPrice;
        }

        private void BuyRoomsDelegate(Room newRoom, int numberOfRooms) 
        {
            for (var i = 0; i < numberOfRooms; i++)
            {
                Rooms.Add(newRoom);
            }
            parent.parentForm.RoomCount = Rooms.Count();
            if(newRoom.GetType() == typeof(RoomDelux))
            {
                parent.parentForm.RoomDeluxCount += numberOfRooms;
            }
        }
    }
}