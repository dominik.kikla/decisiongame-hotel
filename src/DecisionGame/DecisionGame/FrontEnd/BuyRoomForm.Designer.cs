﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DecisionGame.FrontEnd
{
    partial class BuyRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.BuyRoomButton = new System.Windows.Forms.Button();
            this.IsDeluxCheckBox = new System.Windows.Forms.CheckBox();
            this.PriceLabel = new System.Windows.Forms.Label();
            this.RoomsAllowedLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.NewRoomsCountLabel = new System.Windows.Forms.Label();
            this.NewRoomsCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.MoneyLabel = new System.Windows.Forms.Label();
            this.AlertLabel = new System.Windows.Forms.Label();
            this.SummaryCostLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize) (this.NewRoomsCountNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // BuyRoomButton
            // 
            this.BuyRoomButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.BuyRoomButton.Location = new System.Drawing.Point(100, 340);
            this.BuyRoomButton.Margin = new System.Windows.Forms.Padding(4);
            this.BuyRoomButton.Name = "BuyRoomButton";
            this.BuyRoomButton.Size = new System.Drawing.Size(313, 84);
            this.BuyRoomButton.TabIndex = 0;
            this.BuyRoomButton.Text = "Kup Pokój";
            this.BuyRoomButton.UseVisualStyleBackColor = true;
            this.BuyRoomButton.Click += new System.EventHandler(this.BuyRoomButton_Click);
            // 
            // IsDeluxCheckBox
            // 
            this.IsDeluxCheckBox.AutoSize = true;
            this.IsDeluxCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.IsDeluxCheckBox.Location = new System.Drawing.Point(577, 212);
            this.IsDeluxCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.IsDeluxCheckBox.Name = "IsDeluxCheckBox";
            this.IsDeluxCheckBox.Size = new System.Drawing.Size(105, 35);
            this.IsDeluxCheckBox.TabIndex = 1;
            this.IsDeluxCheckBox.Text = "Delux";
            this.IsDeluxCheckBox.UseVisualStyleBackColor = true;
            this.IsDeluxCheckBox.CheckedChanged += new System.EventHandler(this.IsDeluxCheckBox_CheckedChanged);
            // 
            // PriceLabel
            // 
            this.PriceLabel.AutoSize = true;
            this.PriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.PriceLabel.Location = new System.Drawing.Point(32, 102);
            this.PriceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PriceLabel.Name = "PriceLabel";
            this.PriceLabel.Size = new System.Drawing.Size(263, 29);
            this.PriceLabel.TabIndex = 2;
            this.PriceLabel.Text = "Cena jednostkowa: 500";
            // 
            // RoomsAllowedLabel
            // 
            this.RoomsAllowedLabel.AutoSize = true;
            this.RoomsAllowedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.RoomsAllowedLabel.Location = new System.Drawing.Point(149, 47);
            this.RoomsAllowedLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.RoomsAllowedLabel.Name = "RoomsAllowedLabel";
            this.RoomsAllowedLabel.Size = new System.Drawing.Size(406, 25);
            this.RoomsAllowedLabel.TabIndex = 3;
            this.RoomsAllowedLabel.Text = "Możesz jeszcze kupić 100 pokoi w tej rundzie";
            // 
            // CloseButton
            // 
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.CloseButton.Location = new System.Drawing.Point(428, 340);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(4);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(197, 84);
            this.CloseButton.TabIndex = 4;
            this.CloseButton.Text = "Zamknij";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // NewRoomsCountLabel
            // 
            this.NewRoomsCountLabel.AutoSize = true;
            this.NewRoomsCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.NewRoomsCountLabel.Location = new System.Drawing.Point(35, 212);
            this.NewRoomsCountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.NewRoomsCountLabel.Name = "NewRoomsCountLabel";
            this.NewRoomsCountLabel.Size = new System.Drawing.Size(336, 29);
            this.NewRoomsCountLabel.TabIndex = 5;
            this.NewRoomsCountLabel.Text = "Ile pokoi chcesz kupić (1-100):";
            // 
            // NewRoomsCountNumericUpDown
            // 
            this.NewRoomsCountNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.NewRoomsCountNumericUpDown.Location = new System.Drawing.Point(393, 212);
            this.NewRoomsCountNumericUpDown.Margin = new System.Windows.Forms.Padding(4);
            this.NewRoomsCountNumericUpDown.Name = "NewRoomsCountNumericUpDown";
            this.NewRoomsCountNumericUpDown.Size = new System.Drawing.Size(160, 34);
            this.NewRoomsCountNumericUpDown.TabIndex = 7;
            this.NewRoomsCountNumericUpDown.ValueChanged += new System.EventHandler(this.NewRoomsCountNumericUpDown_ValueChanged);
            // 
            // MoneyLabel
            // 
            this.MoneyLabel.AutoSize = true;
            this.MoneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.MoneyLabel.Location = new System.Drawing.Point(35, 156);
            this.MoneyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MoneyLabel.Name = "MoneyLabel";
            this.MoneyLabel.Size = new System.Drawing.Size(126, 29);
            this.MoneyLabel.TabIndex = 8;
            this.MoneyLabel.Text = "Pieniądze:";
            // 
            // AlertLabel
            // 
            this.AlertLabel.AutoSize = true;
            this.AlertLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.AlertLabel.ForeColor = System.Drawing.Color.Red;
            this.AlertLabel.Location = new System.Drawing.Point(180, 270);
            this.AlertLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.AlertLabel.Name = "AlertLabel";
            this.AlertLabel.Size = new System.Drawing.Size(356, 24);
            this.AlertLabel.TabIndex = 9;
            this.AlertLabel.Text = "Niewystarczająco pieniędzy na tyle pokoi!";
            this.AlertLabel.Visible = false;
            // 
            // SummaryCostLabel
            // 
            this.SummaryCostLabel.AutoSize = true;
            this.SummaryCostLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.SummaryCostLabel.Location = new System.Drawing.Point(388, 156);
            this.SummaryCostLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SummaryCostLabel.Name = "SummaryCostLabel";
            this.SummaryCostLabel.Size = new System.Drawing.Size(94, 29);
            this.SummaryCostLabel.TabIndex = 10;
            this.SummaryCostLabel.Text = "Razem:";
            // 
            // BuyRoomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 457);
            this.Controls.Add(this.SummaryCostLabel);
            this.Controls.Add(this.AlertLabel);
            this.Controls.Add(this.MoneyLabel);
            this.Controls.Add(this.NewRoomsCountNumericUpDown);
            this.Controls.Add(this.NewRoomsCountLabel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.RoomsAllowedLabel);
            this.Controls.Add(this.PriceLabel);
            this.Controls.Add(this.IsDeluxCheckBox);
            this.Controls.Add(this.BuyRoomButton);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "BuyRoomForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hotel Stars [kupowanie pokoju]";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BuyRoomForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.BuyRoomForm_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize) (this.NewRoomsCountNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private Button BuyRoomButton;
        private CheckBox IsDeluxCheckBox;
        private Label PriceLabel;
        private Label RoomsAllowedLabel;
        private Button CloseButton;
        private Label NewRoomsCountLabel;
        private NumericUpDown NewRoomsCountNumericUpDown;
        private Label MoneyLabel;
        private Label AlertLabel;
        private Label SummaryCostLabel;
    }
}