﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public partial class BuyRoomForm : Form {
        public int CostPerRoomWithRabat;

        public int OneRoomCost;
        public GameForm ParentGameForm;
        public BuyRoomForm(GameForm parent) {
            InitializeComponent();

            ParentGameForm = parent;

            NewRoomsCountNumericUpDown.Maximum = 100;
        }

        private void IsDeluxCheckBoxCheckedChanged() {
            OneRoomCost = IsDeluxCheckBox.Checked ? RoomDelux.StaticBuyCost() : RoomStandard.StaticBuyCost();
            updateCostPerRoom();
        }

        private void IsDeluxCheckBox_CheckedChanged(object sender, EventArgs e) {
            IsDeluxCheckBoxCheckedChanged();
        }

        private void updateCostPerRoom() {
            var roomsCount = (int) NewRoomsCountNumericUpDown.Value;
            CostPerRoomWithRabat = Room.CalculateCostPerRoom(OneRoomCost, roomsCount);
            PriceLabel.Text =
                $"Cena jednostkowa (rabat = {Math.Round((double) ((OneRoomCost - CostPerRoomWithRabat) * 100 / OneRoomCost), 2)} %): {CostPerRoomWithRabat}";
            SummaryCostLabel.Text = $"Razem: {roomsCount * CostPerRoomWithRabat}";
        }

        private void BuyRoomButton_Click(object sender, EventArgs e) {
            var newRoomsCount = (int) NewRoomsCountNumericUpDown.Value;
            if (ParentGameForm.Game.AllowedRoomsToBuyCount - newRoomsCount < 0) {
                MessageBox.Show(
                    "Nie możesz kupić więcej niż 100 pokoi łącznie w rundzie!"); // byc moze jakies wywołanie specjalnego
                //komunikatu, ten messagebox roboczo
                NewRoomsCountNumericUpDown.Value = ParentGameForm.Game.AllowedRoomsToBuyCount;
                return;
            }

            if (ParentGameForm.Game.Hotel.Money < CostPerRoomWithRabat * newRoomsCount) {
                MessageBox.Show("Nie masz pieniędzy na tyle pokoi!");
                return;
            }

            ParentGameForm.Game.AllowedRoomsToBuyCount -= newRoomsCount;

            ParentGameForm.Game.Hotel.BuyRooms(IsDeluxCheckBox.Checked ? (Room) new RoomDelux() : new RoomStandard(),
                (int) NewRoomsCountNumericUpDown.Value, CostPerRoomWithRabat);
        }

        private void CloseButton_Click(object sender, EventArgs e) {
            Hide();
        }

        private void BuyRoomForm_VisibleChanged(object sender, EventArgs e) {
            if (Visible) {
                ParentGameForm.Location = new Point(
                    (Screen.FromControl(this).WorkingArea.Width - ParentGameForm.Width - this.Width) / 2,
                    (Screen.FromControl(this).WorkingArea.Height - ParentGameForm.Height) / 2
                );
                Location = new Point(
                    ParentGameForm.Location.X + ParentGameForm.Width,
                    (Screen.FromControl(this).WorkingArea.Height - Height) / 2
                );
            }
            else {
                ParentGameForm.backToStandardLocation();
                return;
            }

            adjustForm();
        }

        private void BuyRoomForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (e.CloseReason != CloseReason.UserClosing) return;
            e.Cancel = true;
            Hide();
        }

        private void adjustForm() {
            IsDeluxCheckBox.Visible = ParentGameForm.Game.CurrentRound.RoundNumber >= Game.numberRoomsDeluxBecomeAvailable;
            MoneyLabel.Text = $"Pieniądze: {ParentGameForm.Game.Hotel.Money}";

            IsDeluxCheckBoxCheckedChanged();
        }
        private void NewRoomsCountNumericUpDown_ValueChanged(object sender, EventArgs e) {
            AlertLabel.Visible =
                ParentGameForm.Game.Hotel.Money < CostPerRoomWithRabat * ((NumericUpDown) sender).Value;
            updateCostPerRoom();
        }

        public void ChangeAllowedRoomsToBuyCount() {
            RoomsAllowedLabel.Text =
                $"Możesz jeszcze kupić {ParentGameForm.Game.AllowedRoomsToBuyCount} pokoi w tej rundzie";
            NewRoomsCountLabel.Text = $"Ile pokoi chcesz kupić (1-{ParentGameForm.Game.AllowedRoomsToBuyCount}):";
        }

        public void ChangedMoneyEvent() {
            MoneyLabel.Text = $"Pieniądze: {ParentGameForm.Game.Hotel.Money}";
        }
    }
}