﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public abstract class DecisionsWithControlsBase 
    {

        public static int decisions = 0;
        public Game Owner;

        public DecisionsWithControlsBase(Game owner) 
        {
            Owner = owner;
        }

        protected int getIndex() {
            ++decisions;
            return decisions;
        }

        public abstract Decision[] getDecisions();
    }
}