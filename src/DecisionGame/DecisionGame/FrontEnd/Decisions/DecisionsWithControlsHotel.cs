﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public class DecisionsWithControlsHotel : DecisionsWithControlsBase {
        public Decision ChangePricePerRoomDecision;
        public Decision ChangePricePerRoomDeluxDecision;

        public DecisionsWithControlsHotel(Game owner) : base(owner)
        {
            changePricePerRoomDecision();
            changePricePerRoomDecisionDelux();
        }

        private void changePricePerRoomDecision() {
            ChangePricePerRoomDecision = new Decision 
            {
                IsPermament = true,
                Owner = Owner.parentForm.hotelForm,
                Index = getIndex(),
                Completed = false,
                defaultDecision = null
            };

            var roomPrice = new GroupBox();
            roomPrice.Text = "Cena za noc w pokoju";
            roomPrice.Size = new Size(400, 200);

            var priceButton = new Button();
            priceButton.Text = "Zmień cenę";
            priceButton.Location = new Point((roomPrice.Width - priceButton.Width) / 2, roomPrice.Height - priceButton.Height - 20);

            var priceNumericUpDown = new NumericUpDown();
            priceNumericUpDown.Maximum = int.MaxValue;
            priceNumericUpDown.Value = RoomStandard.CostPerDay;
            priceNumericUpDown.Location = new Point((roomPrice.Width - priceNumericUpDown.Width) / 2, 50);
            priceNumericUpDown.Increment = 10;

            var priceLabel = new Label();
            priceLabel.AutoSize = true;
            priceLabel.Text = "Podaj pożądaną kwotę za noc w pokoju standard:";
            priceLabel.Location = new Point((priceLabel.Width) / 2, 20);

            void priceButton_onClick(object sender, EventArgs e) 
            {
                Owner.Hotel.makeChangePricePerRoomDecision((int) priceNumericUpDown.Value);
                MessageBox.Show("Cena pokoju standard została zmieniona!");
            }

            priceButton.Click += priceButton_onClick;

            roomPrice.Controls.Add(priceButton);
            roomPrice.Controls.Add(priceNumericUpDown);
            roomPrice.Controls.Add(priceLabel);

            ChangePricePerRoomDecision.Content = roomPrice;
        }

        private void changePricePerRoomDecisionDelux()
        {
            ChangePricePerRoomDeluxDecision = new Decision
            {
                IsPermament = true,
                Owner = Owner.parentForm.hotelForm,
                Index = getIndex(),
                Completed = false,
                defaultDecision = null
            };

            var roomPrice = new GroupBox();
            roomPrice.Text = "Cena za noc w pokoju";
            roomPrice.Size = new Size(400, 200);

            var priceButton = new Button();
            priceButton.Text = "Zmień cenę";
            priceButton.Location = new Point((roomPrice.Width - priceButton.Width) / 2, roomPrice.Height - priceButton.Height - 20);

            var priceNumericUpDown = new NumericUpDown();
            priceNumericUpDown.Maximum = int.MaxValue;
            priceNumericUpDown.Value = RoomDelux.CostPerDay;
            priceNumericUpDown.Location = new Point((roomPrice.Width - priceNumericUpDown.Width) / 2, 50);
            priceNumericUpDown.Increment = 10;

            var priceLabel = new Label();
            priceLabel.AutoSize = true;
            priceLabel.Text = "Podaj pożądaną kwotę za noc w pokoju delux:";
            priceLabel.Location = new Point((priceLabel.Width) / 2, 20);

            void priceButton_onClick(object sender, EventArgs e)
            {
                Owner.Hotel.makeChangePricePerRoomDecision((int)priceNumericUpDown.Value);
                MessageBox.Show("Cena pokoju delux została zmieniona!");
            }

            priceButton.Click += priceButton_onClick;

            roomPrice.Controls.Add(priceButton);
            roomPrice.Controls.Add(priceNumericUpDown);
            roomPrice.Controls.Add(priceLabel);

            ChangePricePerRoomDeluxDecision.Content = roomPrice;
        }

        override public Decision[] getDecisions() {
            return new[] {
                ChangePricePerRoomDecision,
                ChangePricePerRoomDeluxDecision,
            };
        }
    }
}