﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DecisionGame.FrontEnd
{
    public partial class EndGame : Form
    {
        public EndGame(double result, string message)
        {
            InitializeComponent();
            label1.Text = "Twój wynik: " + Math.Round(result).ToString();
            label1.Location = new Point((this.Width - label1.Width) / 2, label1.Location.Y);
            label2.Text = message;
            label2.Location = new Point((this.Width - label2.Width) / 2, label2.Location.Y);
        }

        private void EndGame_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
