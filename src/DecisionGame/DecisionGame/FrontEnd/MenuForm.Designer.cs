﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DecisionGame.FrontEnd
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
            this.StartButton = new System.Windows.Forms.Button();
            this.PlayerNameLabel = new System.Windows.Forms.Label();
            this.PlayerNameTextBox = new System.Windows.Forms.TextBox();
            this.SuburbsRadioButton = new System.Windows.Forms.RadioButton();
            this.SuburbsPictureBox = new System.Windows.Forms.PictureBox();
            this.MiddleCityPictureBox = new System.Windows.Forms.PictureBox();
            this.MiddleCityRadioButton = new System.Windows.Forms.RadioButton();
            this.CenterPictureBox = new System.Windows.Forms.PictureBox();
            this.CenterRadioButton = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.SuburbsPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MiddleCityPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CenterPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // StartButton
            // 
            this.StartButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_green;
            this.StartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.StartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.StartButton.Location = new System.Drawing.Point(192, 379);
            this.StartButton.Margin = new System.Windows.Forms.Padding(2);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(389, 141);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "START";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // PlayerNameLabel
            // 
            this.PlayerNameLabel.AutoSize = true;
            this.PlayerNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerNameLabel.Location = new System.Drawing.Point(188, 130);
            this.PlayerNameLabel.Name = "PlayerNameLabel";
            this.PlayerNameLabel.Size = new System.Drawing.Size(132, 24);
            this.PlayerNameLabel.TabIndex = 1;
            this.PlayerNameLabel.Text = "Nazwa Hotelu:";
            // 
            // PlayerNameTextBox
            // 
            this.PlayerNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerNameTextBox.Location = new System.Drawing.Point(350, 127);
            this.PlayerNameTextBox.Name = "PlayerNameTextBox";
            this.PlayerNameTextBox.Size = new System.Drawing.Size(231, 29);
            this.PlayerNameTextBox.TabIndex = 2;
            // 
            // SuburbsRadioButton
            // 
            this.SuburbsRadioButton.AutoSize = true;
            this.SuburbsRadioButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SuburbsRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SuburbsRadioButton.Location = new System.Drawing.Point(91, 217);
            this.SuburbsRadioButton.MaximumSize = new System.Drawing.Size(200, 500);
            this.SuburbsRadioButton.Name = "SuburbsRadioButton";
            this.SuburbsRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SuburbsRadioButton.Size = new System.Drawing.Size(171, 29);
            this.SuburbsRadioButton.TabIndex = 5;
            this.SuburbsRadioButton.Text = "Przedmieścia  \r\n";
            this.SuburbsRadioButton.UseVisualStyleBackColor = false;
            this.SuburbsRadioButton.CheckedChanged += new System.EventHandler(this.SuburbsRadioButton_CheckedChanged);
            // 
            // SuburbsPictureBox
            // 
            this.SuburbsPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SuburbsPictureBox.BackgroundImage")));
            this.SuburbsPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SuburbsPictureBox.Location = new System.Drawing.Point(89, 252);
            this.SuburbsPictureBox.Name = "SuburbsPictureBox";
            this.SuburbsPictureBox.Size = new System.Drawing.Size(161, 110);
            this.SuburbsPictureBox.TabIndex = 6;
            this.SuburbsPictureBox.TabStop = false;
            this.SuburbsPictureBox.Click += new System.EventHandler(this.SuburbsPictureBox_Click);
            // 
            // MiddleCityPictureBox
            // 
            this.MiddleCityPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MiddleCityPictureBox.BackgroundImage")));
            this.MiddleCityPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MiddleCityPictureBox.Location = new System.Drawing.Point(306, 252);
            this.MiddleCityPictureBox.Name = "MiddleCityPictureBox";
            this.MiddleCityPictureBox.Size = new System.Drawing.Size(161, 110);
            this.MiddleCityPictureBox.TabIndex = 8;
            this.MiddleCityPictureBox.TabStop = false;
            this.MiddleCityPictureBox.Click += new System.EventHandler(this.MiddleCityPictureBox_Click);
            // 
            // MiddleCityRadioButton
            // 
            this.MiddleCityRadioButton.AutoSize = true;
            this.MiddleCityRadioButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MiddleCityRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MiddleCityRadioButton.Location = new System.Drawing.Point(306, 217);
            this.MiddleCityRadioButton.MaximumSize = new System.Drawing.Size(200, 500);
            this.MiddleCityRadioButton.Name = "MiddleCityRadioButton";
            this.MiddleCityRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MiddleCityRadioButton.Size = new System.Drawing.Size(172, 29);
            this.MiddleCityRadioButton.TabIndex = 7;
            this.MiddleCityRadioButton.Text = "Śródmieście    ";
            this.MiddleCityRadioButton.UseVisualStyleBackColor = false;
            this.MiddleCityRadioButton.CheckedChanged += new System.EventHandler(this.MiddleCityRadioButton_CheckedChanged);
            // 
            // CenterPictureBox
            // 
            this.CenterPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CenterPictureBox.BackgroundImage")));
            this.CenterPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CenterPictureBox.Location = new System.Drawing.Point(526, 252);
            this.CenterPictureBox.Name = "CenterPictureBox";
            this.CenterPictureBox.Size = new System.Drawing.Size(161, 110);
            this.CenterPictureBox.TabIndex = 10;
            this.CenterPictureBox.TabStop = false;
            this.CenterPictureBox.Click += new System.EventHandler(this.CenterPictureBox_Click);
            // 
            // CenterRadioButton
            // 
            this.CenterRadioButton.AutoSize = true;
            this.CenterRadioButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CenterRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CenterRadioButton.Location = new System.Drawing.Point(526, 217);
            this.CenterRadioButton.MaximumSize = new System.Drawing.Size(200, 500);
            this.CenterRadioButton.Name = "CenterRadioButton";
            this.CenterRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CenterRadioButton.Size = new System.Drawing.Size(177, 29);
            this.CenterRadioButton.TabIndex = 9;
            this.CenterRadioButton.Text = "Centrum           ";
            this.CenterRadioButton.UseVisualStyleBackColor = false;
            this.CenterRadioButton.CheckedChanged += new System.EventHandler(this.CenterRadioButton_CheckedChanged);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(818, 578);
            this.Controls.Add(this.CenterPictureBox);
            this.Controls.Add(this.CenterRadioButton);
            this.Controls.Add(this.MiddleCityPictureBox);
            this.Controls.Add(this.MiddleCityRadioButton);
            this.Controls.Add(this.SuburbsPictureBox);
            this.Controls.Add(this.SuburbsRadioButton);
            this.Controls.Add(this.PlayerNameTextBox);
            this.Controls.Add(this.PlayerNameLabel);
            this.Controls.Add(this.StartButton);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hotel Stars";
            ((System.ComponentModel.ISupportInitialize)(this.SuburbsPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MiddleCityPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CenterPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.Button StartButton;

        #endregion

        private System.Windows.Forms.Label PlayerNameLabel;
        private TextBox PlayerNameTextBox;
        private System.Windows.Forms.RadioButton SuburbsRadioButton;
        private PictureBox SuburbsPictureBox;
        private PictureBox MiddleCityPictureBox;
        private System.Windows.Forms.RadioButton MiddleCityRadioButton;
        private System.Windows.Forms.PictureBox CenterPictureBox;
        private System.Windows.Forms.RadioButton CenterRadioButton;
    }
}