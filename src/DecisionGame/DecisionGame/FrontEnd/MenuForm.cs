﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public partial class MenuForm : Form {
        private readonly InitialSettings settings;
        private GameForm gameForm;
        private Image  defaultBgImage;
        public MenuForm() {
            InitializeComponent();
            settings = new InitialSettings();
            defaultBgImage = BackgroundImage;
        }
        private void StartButton_Click(object sender, EventArgs e) {
            startGame();
        }

        private void startGame() {
            var img = collectData();

            if (!validate()) return;

            Hide();

            gameForm = new GameForm(settings, img);
        }

        private Image collectData() {
            settings.PlayerName = PlayerNameTextBox.Text;
            if (SuburbsRadioButton.Checked) {
                settings.Place = Place.Suburbs;
                return SuburbsPictureBox.BackgroundImage;
            }

            if (MiddleCityRadioButton.Checked) {
                settings.Place = Place.MiddleCity;
                return MiddleCityPictureBox.BackgroundImage;
            }

            if (CenterRadioButton.Checked) {
                settings.Place = Place.Center;
                return CenterPictureBox.BackgroundImage;
            }

            return SuburbsPictureBox.BackgroundImage;
        }

        private bool validate() {
            if (settings.PlayerName == "") {
                MessageBox.Show("Nazwa gracza nie może być pusta!");
                return false;
            }

            if (settings.Place == Place.None) {
                MessageBox.Show("Musisz wybrać początkową lokalizację hotelu");
                return false;
            }

            return true;
        }

        private void SuburbsPictureBox_Click(object sender, EventArgs e) {
            SuburbsRadioButton.Checked = true;
            BackgroundImage = SuburbsPictureBox.BackgroundImage;
        }

        private void MiddleCityPictureBox_Click(object sender, EventArgs e) {
            MiddleCityRadioButton.Checked = true;
            BackgroundImage = MiddleCityPictureBox.BackgroundImage;
        }

        private void CenterPictureBox_Click(object sender, EventArgs e) {
            CenterRadioButton.Checked = true;
            BackgroundImage = CenterPictureBox.BackgroundImage;
        }
        private void SuburbsRadioButton_CheckedChanged(object sender, EventArgs e) {
            BackgroundImage = BackgroundImage.Equals(defaultBgImage) ? SuburbsPictureBox.BackgroundImage : defaultBgImage;
        }
        private void MiddleCityRadioButton_CheckedChanged(object sender, EventArgs e) {
            BackgroundImage = BackgroundImage.Equals(defaultBgImage) ? MiddleCityPictureBox.BackgroundImage : defaultBgImage;
        }
        private void CenterRadioButton_CheckedChanged(object sender, EventArgs e) {
            BackgroundImage = BackgroundImage.Equals(defaultBgImage) ? CenterPictureBox.BackgroundImage : defaultBgImage;
        }
    }
}