﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public partial class BaseForm : Form {
        public Dictionary<int, Decision> decisions;
        private bool decisionView;
        private int newDecisions;
        public GameForm parent;

        public BaseForm() {
            InitializeComponent();
        }

        public BaseForm(GameForm parent1) {
            parent = parent1;
            decisions = new Dictionary<int, Decision>();

            
            InitializeComponent();
            //this.ClientSize = new Size(Screen.FromControl(parent).WorkingArea.Width + 800, this.Height);

            //Width = Screen.FromControl(parent).WorkingArea.Width - parent.Width;

            //this.Size = new Size(this.Width - 200, this.Height);

            DecisionPanel.AutoScroll = true;

            DecisionPanel.Size = new Size((int) (DecisionPanel.Parent.Width * 0.9), Height - menuStrip1.Height - 50);
            DecisionPanel.Location = new Point((int) (DecisionPanel.Parent.Width * 0.05), menuStrip1.Height + 10);

            PerformancePanel.Size = new Size((int) (PerformancePanel.Parent.Width * 0.9), PerformancePanel.Height);
            PerformancePanel.Location = new Point((int) (PerformancePanel.Parent.Width * 0.05), menuStrip1.Height + 10);
            
            DecisionView = true;
        }

        public bool DecisionView {
            get => decisionView;
            set {
                DecisionPanel.Visible = value;
                PerformancePanel.Visible = !value;
                decisionView = value;
            }
        }

        public int NewDecisions {
            get => newDecisions;
            set {
                if (value == 0) {
                    DecisionsToolStripMenuItem.ForeColor = Color.Black;
                    DecisionsToolStripMenuItem.Text = "decyzje";
                }
                else {
                    DecisionsToolStripMenuItem.ForeColor = Color.Red;
                    DecisionsToolStripMenuItem.Text = "decyzje (!)";
                }

                newDecisions = value;
            }
        }

        public void AddDecision(Decision newDecisionsToMake) {
            ++NewDecisions;
            DecisionPanel.AutoScrollMinSize = new Size(0, DecisionPanel.AutoScrollMinSize.Height + newDecisionsToMake.Content.Height + 10);

            foreach (Control control in DecisionPanel.Controls)
                control.Location = new Point(control.Location.X, control.Location.Y + newDecisionsToMake.Content.Height + 10);

            newDecisionsToMake.Content.Location = new Point((DecisionPanel.Width - newDecisionsToMake.Content.Width) >> 1, 0);
            DecisionPanel.Controls.Add(newDecisionsToMake.Content);

            decisions.Add(newDecisionsToMake.Index, newDecisionsToMake);
        }

        public void RemoveDecision(int index) {
            var controlToRemove = decisions[index].Content;
            foreach (Control control in DecisionPanel.Controls)
                if (control.Location.Y > controlToRemove.Location.Y)
                    control.Location = new Point(control.Location.X, control.Location.Y - controlToRemove.Height - 10);

            DecisionPanel.Controls.Remove(controlToRemove);
            decisions.Remove(index);
        }

        public void MakeDecision(int index) {
            if (decisions.ContainsKey(index) && !decisions[index].Completed) {
                --NewDecisions;
                decisions[index].Completed = true;
            }
        }

        public void PrepareForNextRound() {
            var toDel = new List<int>();
            foreach (var decision in decisions.Values)
                if (!decision.IsPermament) {
                    if (!decision.Completed && decision.defaultDecision != null) decision.defaultDecision();
                    toDel.Add(decision.Index);
                }

            foreach (var i in toDel) RemoveDecision(i);
        }

        public void Alert(string msg) {
            MessageBox.Show(msg);
        }

        private void BaseForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) {
                e.Cancel = true;
                Hide();
            }
        }

        private void DecisionsToolStripMenuItem_Click(object sender, EventArgs e) {
            DecisionView = true;
        }

        private void PerformanceToolStripMenuItem_Click(object sender, EventArgs e) {
            DecisionView = false;
        }

        private void BaseForm_VisibleChanged(object sender, EventArgs e) {
            if (Visible) {
                parent.Location = new Point(
                    (Screen.FromControl(this).WorkingArea.Width - parent.Width - this.Width) / 2,
                    (Screen.FromControl(this).WorkingArea.Height - parent.Height) / 2
                );
                Location = new Point(
                    parent.Location.X + parent.Width,
                    (Screen.FromControl(this).WorkingArea.Height - Height) / 2
                );
            }
            else {
                parent.backToStandardLocation();
            }
        }
    }
}