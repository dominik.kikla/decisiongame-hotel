﻿using System;
using System.Collections.Generic;
using DecisionGame.BackEnd;
using DecisionGame.FrontEnd;

namespace DecisionGame.Utils {
    public class Rounds {
        public static Round[] getRounds(Game game) {
            var decisions = new DecisionsWithControls(game).getDecisions();
            var decisionsHotel = new DecisionsWithControlsHotel(game).getDecisions();
            var decisionsWorkOffice = new DecisionsWithControlsWorkOffice(game).getDecisions();
            var decisionsAdAgency = new DecisionsWithControlsAdAgency(game).getDecisions();

            return new[] {
                new Round(1, new List<Event> {
                        // Dofinansowanie z Unii 1/4
                        new Event(new List<Action> {
                            () => Event.ShowMessage(
                                "Dostajesz dofinansowanie z Unii Europejskiej na rozpoczęcie i rozwój działalności gospodarczej. (4 transze po 75 000 PLN w ciągu pierwszego roku)"),
                            () => Event.AddMoney(75000, game.Hotel)
                        })
                    },
                    new List<Decision> {
                        decisions[0],
                        //decisions[1],
                        decisionsHotel[0],
                        decisionsWorkOffice[0]
                    }
                ),
                new Round(2, new List<Event> {
                    // Dofinansowanie z Unii 2/4
                    new Event(new List<Action> {
                        () => Event.AddMoney(75000, game.Hotel)
                    }),
                    // Festiwal filmowy 1/2
                    new Event(new List<Action> {
                        () => Event.ShowMessage(
                            "W mieście organizowany jest festiwal filmowy, w związku z czym popularność hotelu w najbliższym czasie znacznie wzrasta."),
                        () => Event.AddPopularity(100, game.Hotel)
                    })
                }),
                new Round(3, new List<Event> {
                    // Dofinansowanie z Unii 3/4
                    new Event(new List<Action> {
                        () => Event.AddMoney(75000, game.Hotel)
                    }),
                    // Festiwal filmowy 2/2
                    new Event(new List<Action> {
                        () => Event.AddPopularity(-100, game.Hotel)
                    }),
                    // Wzrost cen wody 1/1
                    new Event(new List<Action> {
                        () => Event.ShowMessage("W związku z wzrostem cen wody w mieście podatek zwiększa się o 10%"),
                        () => Event.ShowMessage("Pojawiła się jednorazowa oferta specjalnej reklamy, sprawdź agencję reklamy"),
                        () => Event.AddMonthlyPayments((int) 0.1 * game.Hotel.MonthlyPayments, game.Hotel)
                    })
                    },
                    new List<Decision> {
                        decisionsAdAgency[0],
                        decisionsAdAgency[1]
                    }
                ),
                new Round(4, new List<Event> {
                    // Dofinansowanie z Unii 4/4
                    new Event(new List<Action> {
                        () => Event.AddMoney(75000, game.Hotel)
                    })
                }),
                new Round(5, new List<Event> {
                    new Event(new List<Action> {
                        () => Event.ShowMessage(
                            "Od teraz możesz kupić pokoje Delux charakteryzujące się wyższą ceną, ale i jakością.")
                    })
                    },
                    new List<Decision> {
                        decisionsHotel[1]
                    }
                ),
                new Round(6, new List<Event> {
                    new Event(new List<Action>())
                }),
                new Round(7, new List<Event> {
                    // Mistrzostwa Europy w Piłce Siatkowej 1/2
                    new Event(new List<Action> {
                        () => Event.ShowMessage("Ze względu na Mistrzostwa Świata w Piłce Siatkowej znacznie wzrasta popyt na usługi hotelarskie."),
                        () => Event.AddPopularity(100, game.Hotel)
                    })
                }),
                new Round(8, new List<Event> {
                    // Mistrzostwa Europy w Piłce Siatkowej 1/2
                    new Event(new List<Action> {
                        () => Event.AddPopularity(-100, game.Hotel)
                    }),
                    // Wprowadzenie opłaty klimatycznej
                    new Event(new List<Action> {
                        () => Event.ShowMessage("W mieście zostaje wprowadzona opłata klimatyczna w wysokości 2 PLN za osobonoc."),
                        () => Event.AddRoomsCost(2)
                    })
                }),
                new Round(9, new List<Event> {
                    new Event(new List<Action>())
                }),
                new Round(10, new List<Event> {
                    new Event(new List<Action>())
                    }
                ),
                new Round(11, new List<Event> {
                    // Zbiórka charytatywna 1/2
                    new Event(new List<Action> {
                        () => Event.ShowMessage(
                            "Władze miasta zorganizowały zbiórkę charytatywną, uczestnictwo pozwoliło na czasowe zwiększenie popularności Twojego hotelu."),
                        () => Event.AddMoney(-15000, game.Hotel),
                        () => Event.AddPopularity(25, game.Hotel)
                    })
                }),
                new Round(12, new List<Event> {
                    // Zbiórka charytatywna 2/2
                    new Event(new List<Action> {
                        () => Event.AddPopularity(-25, game.Hotel)
                    })
                }),
                new Round(13, new List<Event> {
                    new Event(new List<Action>())
                }),
                new Round(14, new List<Event> {
                    new Event(new List<Action>())
                }),
                new Round(15, new List<Event> {
                    new Event(new List<Action>())
                }),
                new Round(16, new List<Event> {
                    new Event(new List<Action>())
                })
            };
        }
    }
}